package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ElfoRepository  extends PersonagemRepository<ElfoEntity> {
}
