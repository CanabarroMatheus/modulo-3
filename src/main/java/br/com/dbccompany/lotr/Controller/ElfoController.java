package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/elfo")
public class ElfoController {
    @Autowired
    private ElfoService service;

    @GetMapping
    @ResponseBody
    public List<ElfoDTO> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ElfoDTO findById(@PathVariable Integer id) {
        return this.service.findById(id);
    }

    @PostMapping
    @ResponseBody
    public ElfoDTO save(@RequestBody ElfoDTO elfo) {
        return this.service.save(elfo.converter());
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ElfoDTO update(@RequestBody ElfoEntity elfoEntity, @PathVariable Integer id) {
        return this.service.update(elfoEntity, id);
    }
}
