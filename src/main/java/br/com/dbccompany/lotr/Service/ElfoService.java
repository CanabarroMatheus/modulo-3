package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService {

    @Autowired
    private ElfoRepository repository;

    public List<ElfoDTO> findAll() {
        List<ElfoEntity> elfos = (List<ElfoEntity>) this.repository.findAll();
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByNomeIn(List<String> nomes) {
        List<ElfoEntity> elfos = this.repository.findAllByNomeIn(nomes);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByExperienciaIn(List<Integer> experiencias) {
        List<ElfoEntity> elfos = this.repository.findAllByExperienciaIn(experiencias);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByVidaIn(List<Double> vida) {
        List<ElfoEntity> elfos = this.repository.findAllByVidaIn(vida);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByQuantidadeDanoIn(List<Double> quantidadesDano) {
        List<ElfoEntity> elfos = this.repository.findAllByQuantidadeDanoIn(quantidadesDano);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByQuantidadeExperienciaPorAtaqueIn(List<Integer> quantidadesExperienciaPorAtaque) {
        List<ElfoEntity> elfos = this.repository.findAllByQuantidadeExperienciaPorAtaqueIn(quantidadesExperienciaPorAtaque);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByStatusIn(List<StatusEnum> status) {
        List<ElfoEntity> elfos = this.repository.findAllByStatusIn(status);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByNome(String nome) {
        List<ElfoEntity> elfos = this.repository.findAllByNome(nome);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByExperiencia(Integer experiencia) {
        List<ElfoEntity> elfos = this.repository.findAllByExperiencia(experiencia);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByVida(Double vida) {
        List<ElfoEntity> elfos = this.repository.findAllByVida(vida);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByQuantidadeDano(Double quantidadeDano) {
        List<ElfoEntity> elfos = this.repository.findAllByQuantidadeDano(quantidadeDano);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque) {
        List<ElfoEntity> elfos = this.repository.findAllByQuantidadeExperienciaPorAtaque(quantidadeExperienciaPorAtaque);
        return this.convertEntityListToDTOList(elfos);
    }

    public List<ElfoDTO> findAllByStatus(StatusEnum status) {
        List<ElfoEntity> elfos = this.repository.findAllByStatus(status);
        return this.convertEntityListToDTOList(elfos);
    }

    public ElfoDTO findById(Integer id) {
        ElfoEntity elfo = this.repository.findById(id).orElse(null);
        return new ElfoDTO(elfo);
    }

    public ElfoDTO findByNome(String nome) {
        ElfoEntity elfo = this.repository.findByNome(nome).orElse(null);
        return new ElfoDTO(elfo);
    }

    public ElfoDTO findByExperiencia(Integer experiencia) {
        ElfoEntity elfo = this.repository.findByExperiencia(experiencia).orElse(null);
        return new ElfoDTO(elfo);
    }

    public ElfoDTO findByVida(Double vida) {
        ElfoEntity elfo = this.repository.findByVida(vida).orElse(null);
        return new ElfoDTO(elfo);
    }

    public ElfoDTO findByQuantidadeDano(Double quantidadeDano) {
        ElfoEntity elfo = this.repository.findByQuantidadeDano(quantidadeDano).orElse(null);
        return new ElfoDTO(elfo);
    }

    public ElfoDTO findByQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque) {
        ElfoEntity elfo = this.repository.findByQuantidadeExperienciaPorAtaque(quantidadeExperienciaPorAtaque).orElse(null);
        return new ElfoDTO(elfo);
    }

    public ElfoDTO findByStatus(StatusEnum status) {
        ElfoEntity elfo = this.repository.findByStatus(status).orElse(null);
        return new ElfoDTO(elfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public ElfoDTO save(ElfoEntity elfoEntity) {
        ElfoEntity elfo = this.saveAndUpdate(elfoEntity);
        return new ElfoDTO(elfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public ElfoDTO update(ElfoEntity elfoEntity, Integer id) {
        elfoEntity.setId(id);
        ElfoEntity elfo = this.saveAndUpdate(elfoEntity);
        return new ElfoDTO(elfo);
    }

    private ElfoEntity saveAndUpdate(ElfoEntity elfoEntity) {
        return this.repository.save(elfoEntity);
    }

    private List<ElfoDTO> convertEntityListToDTOList(List<ElfoEntity> elfoEntities) {
        ArrayList<ElfoDTO> elfosDTOS = new ArrayList<>();
        for (ElfoEntity elfoEntity : elfoEntities) {
            elfosDTOS.add(new ElfoDTO(elfoEntity));
        }
        return elfosDTOS;
    }
}
