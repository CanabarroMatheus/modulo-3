package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventarioService {

    @Autowired
    InventarioRepository repository;

    public List<InventarioDTO> findAll() {
        List<InventarioEntity> inventarioEntities = (List<InventarioEntity>) this.repository.findAll();
        return this.convertEntityListToDTOList(inventarioEntities);
    }

    public List<InventarioDTO> findAllByPersonagem(PersonagemEntity personagem) {
        List<InventarioEntity> inventarioEntities = this.repository.findAllByPersonagem(personagem);
        return this.convertEntityListToDTOList(inventarioEntities);
    }

    public List<InventarioDTO> findAllByInventarioItem(InventarioXItemEntity inventarioXItemEntity) {
        List<InventarioEntity> inventarioEntities = this.repository.findAllByInventarioItem(inventarioXItemEntity);
        return this.convertEntityListToDTOList(inventarioEntities);
    }

    public List<InventarioDTO> findAllByPersonagemIn(List<PersonagemEntity> personagens) {
        List<InventarioEntity> inventarioEntities = this.repository.findAllByPersonagemIn(personagens);
        return this.convertEntityListToDTOList(inventarioEntities);
    }

    public List<InventarioDTO> findAllByInventarioItemIn(List<InventarioXItemEntity> inventarioXItemEntities) {
        List<InventarioEntity> inventarioEntities = this.repository.findAllByInventarioItemIn(inventarioXItemEntities);
        return this.convertEntityListToDTOList(inventarioEntities);
    }

    public InventarioDTO findById(Integer id) {
        InventarioEntity inventarioEntity = this.repository.findById(id).orElse(null);
        return new InventarioDTO(inventarioEntity);
    }

    public InventarioDTO findByPersonagem(PersonagemEntity personagem) {
        InventarioEntity inventarioEntity = this.repository.findByPersonagem(personagem).orElse(null);
        return new InventarioDTO(inventarioEntity);
    }

    public InventarioDTO findByInventarioItem(InventarioXItemEntity inventarioXItem) {
        InventarioEntity inventarioEntity = this.repository.findByInventarioItem(inventarioXItem).orElse(null);
        return new InventarioDTO(inventarioEntity);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO save(InventarioEntity inventarioEntity) {
        InventarioEntity inventario = this.saveAndUpdate(inventarioEntity);
        return new InventarioDTO(inventario);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO update(InventarioEntity inventarioEntity, Integer id) {
        inventarioEntity.setId(id);
        InventarioEntity inventario = this.saveAndUpdate(inventarioEntity);
        return new InventarioDTO(inventario);
    }

    private InventarioEntity saveAndUpdate(InventarioEntity inventarioEntity) {
        return this.repository.save(inventarioEntity);
    }

    public void delete(InventarioEntity inventarioEntity) {
        this.repository.delete(inventarioEntity);
    }

    public void deleteById(Integer id) {
        this.repository.deleteById(id);
    }

    private List<InventarioDTO> convertEntityListToDTOList(List<InventarioEntity> inventarioEntities) {
        ArrayList<InventarioDTO> inventarioDTOS = new ArrayList<>();
        for (InventarioEntity inventarioEntity : inventarioEntities) {
            inventarioDTOS.add(new InventarioDTO(inventarioEntity));
        }
        return inventarioDTOS;
    }
}
