package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventarioXItemRepository extends CrudRepository<InventarioXItemEntity, InventarioXItemId> {

    Optional<InventarioXItemEntity> findById(InventarioXItemId id);
    Optional<InventarioXItemEntity> findByQuantidade(Integer quantidade);
    List<InventarioXItemEntity> findAllByQuantidade(Integer quantidade);
    List<InventarioXItemEntity> findAllByQuantidadeIn(List<Integer> quantidades);
    Optional<InventarioXItemEntity> findByInventario(Integer idInventario);
    List<InventarioXItemEntity> findAllByInventario(Integer idInventario);
    List<InventarioXItemEntity> findAllByInventarioIn (List<Integer> idsInventario);
    Optional<InventarioXItemEntity> findByItem(Integer id_item);
    List<InventarioXItemEntity> findAllByItem(Integer id_item);
    List<InventarioXItemEntity> findAllByItemIn (List<Integer> ids_item);

}

