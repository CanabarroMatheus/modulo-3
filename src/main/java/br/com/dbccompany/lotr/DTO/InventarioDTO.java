package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;

import java.util.ArrayList;
import java.util.List;

public class InventarioDTO {

    private Integer id;
    //private PersonagemEntity personagem;
    private List<InventarioXItemDTO> inventarioItem;

    public InventarioDTO() {
    }

    public InventarioDTO(InventarioEntity inventario) {
        this.id = inventario.getId();

        ArrayList<InventarioXItemDTO> inventarioItemDTO = new ArrayList<>();

        if (inventario.getInventarioItem() != null) {
            for (InventarioXItemEntity inventarioXItemEntity : inventario.getInventarioItem()) {
                inventarioItemDTO.add(new InventarioXItemDTO(inventarioXItemEntity));
            }
        }

        this.inventarioItem = inventarioItemDTO;
    }

    public InventarioEntity converter() {
        InventarioEntity inventario = new InventarioEntity();
        inventario.setId(this.id);
        ArrayList<InventarioXItemEntity> inventarioXItemEntities = new ArrayList<>();
        if (this.inventarioItem != null) {
            for (InventarioXItemDTO inventarioXItemDTO : this.inventarioItem) {
                inventarioXItemEntities.add(inventarioXItemDTO.converter());
            }
        }
        inventario.setInventarioItem(inventarioXItemEntities);
        return inventario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<InventarioXItemDTO> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<InventarioXItemDTO> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
