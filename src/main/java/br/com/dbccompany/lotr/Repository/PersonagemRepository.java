package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {
    Optional<T> findByNome(String nome);
    List<T> findAllByNome(String nome);
    List<T> findAllByNomeIn(List<String> nomes);
    Optional<T> findByExperiencia(Integer experiencia);
    List<T> findAllByExperiencia(Integer experiencia);
    List<T> findAllByExperienciaIn(List<Integer> experiencias);
    Optional<T> findByVida(Double vida);
    List<T> findAllByVida(Double vida);
    List<T> findAllByVidaIn(List<Double> vidas);
    Optional<T> findByQuantidadeDano(Double quantidadeDano);
    List<T> findAllByQuantidadeDano(Double quantidadeDano);
    List<T> findAllByQuantidadeDanoIn(List<Double> quantidadesDano);
    Optional<T> findByQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque);
    List<T> findAllByQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque);
    List<T> findAllByQuantidadeExperienciaPorAtaqueIn(List<Integer> quantidadesExperienciaPorAtaque);
    Optional<T> findByStatus(StatusEnum status);
    List<T> findAllByStatus(StatusEnum status);
    List<T> findAllByStatusIn(List<StatusEnum> statusList);
}
