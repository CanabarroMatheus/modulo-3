package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {
    Optional<InventarioEntity> findByPersonagem(PersonagemEntity personagens);
    List<InventarioEntity> findAllByPersonagem(PersonagemEntity personagens);
    List<InventarioEntity> findAllByPersonagemIn(List<PersonagemEntity> personagens);
    Optional<InventarioEntity> findByInventarioItem(InventarioXItemEntity inventarioItem);
    List<InventarioEntity> findAllByInventarioItem(InventarioXItemEntity inventarioItem);
    List<InventarioEntity> findAllByInventarioItemIn(List<InventarioXItemEntity> inventariosItem);
}
