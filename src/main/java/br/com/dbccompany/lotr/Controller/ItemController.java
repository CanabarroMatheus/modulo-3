package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/item")
public class ItemController {

    @Autowired
    private ItemService service;

    @GetMapping
    @ResponseBody
    public List<ItemDTO> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public ItemDTO findById(@PathVariable Integer id) {
        return this.service.findById(id);
    }

//    @PostMapping
//    @ResponseBody
//    public ItemEntity save(@RequestBody ItemEntity itemEntity) {
//        return this.service.save(itemEntity);
//    }

    @PostMapping
    @ResponseBody
    public ItemDTO save(@RequestBody ItemDTO item) {
        return this.service.save(item.converter());
    }

//    @PutMapping("/{id}")
//    @ResponseBody
//    public ItemDTO update(@RequestBody ItemEntity itemEntity, @PathVariable Integer id) {
//        return this.service.update(itemEntity, id);
//    }

}
