package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
@DiscriminatorValue("Anao")
public class AnaoEntity extends PersonagemEntity {
    private Boolean escudoEquipado;

    public AnaoEntity() {

    }

    public AnaoEntity(String nome) {
        super(nome);

        super.vida = 110.0;
        this.escudoEquipado = false;
        super.quantidadeDano = 10.0;
    }

    public Boolean getEscudoEquipado() {
        return escudoEquipado;
    }

    public void setEscudoEquipado(Boolean escudoEquipado) {
        this.escudoEquipado = escudoEquipado;
    }
}
