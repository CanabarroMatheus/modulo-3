package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
public class UsuarioEntity {

    @Id
    @SequenceGenerator(name = "USUARIO_SEQUENCE", allocationSize = 1)
    @GeneratedValue(generator = "USUARIO_SEQUENCE", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_usuario")
    private Integer id;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String senha;

    public UsuarioEntity() {
    }

    public UsuarioEntity(String login, String senha) {
        this.login = login;
        this.senha = senha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
