package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {

    Optional<ItemEntity> findByDescricao(String descricao);
    List<ItemEntity> findAllByDescricao(String descricao);
    List<ItemEntity> findAllByDescricaoIn(List<String> descricao);
    Optional<ItemEntity> findByInventarioItem(InventarioXItemEntity inventarioXItemEntity);
    List<ItemEntity> findAllByInventarioItem(InventarioXItemEntity inventarioXItemEntity);
    List<ItemEntity> findAllByInventarioItemIn(List<InventarioXItemEntity> inventarioXItemEntities);

}