package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;

public class AnaoDTO {
    private Integer id;
    private String nome;
    private Integer experiencia;
    private Double vida;
    private Double quantidadeDano;
    private Integer quantidadeExperienciaPorAtaque;
    private StatusEnum status;
    private InventarioDTO inventario;
    private boolean escudoEquipado;

    {
        this.vida = 110.0;
        this.inventario = new InventarioDTO();
        this.status = StatusEnum.RECEM_CRIADO;
        this.quantidadeExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.quantidadeDano = 10.0;
        this.escudoEquipado = false;
    }

    public AnaoDTO() {
    }

    public AnaoDTO(AnaoEntity anao) {
        this.id = anao.getId();
        this.nome = anao.getNome();
        this.experiencia = anao.getExperiencia();
        this.vida = anao.getVida();
        this.quantidadeDano = anao.getQuantidadeDano();
        this.quantidadeExperienciaPorAtaque = anao.getQuantidadeExperienciaPorAtaque();
        this.status = anao.getStatus();
        this.inventario = anao.getInventario() == null ? null : new InventarioDTO(anao.getInventario());
        this.escudoEquipado = anao.getEscudoEquipado();
    }

    public AnaoEntity converter() {
        AnaoEntity anao = new AnaoEntity();
        anao.setId(this.id);
        anao.setNome(this.nome);
        anao.setExperiencia(this.experiencia);
        anao.setVida(this.vida);
        anao.setQuantidadeDano(this.quantidadeDano);
        anao.setQuantidadeExperienciaPorAtaque(this.quantidadeExperienciaPorAtaque);
        anao.setStatus(this.status);
        anao.setInventario(this.inventario.converter());
        anao.setEscudoEquipado(this.escudoEquipado);
        return anao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQuantidadeDano() {
        return quantidadeDano;
    }

    public void setQuantidadeDano(Double quantidadeDano) {
        this.quantidadeDano = quantidadeDano;
    }

    public Integer getQuantidadeExperienciaPorAtaque() {
        return quantidadeExperienciaPorAtaque;
    }

    public void setQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque) {
        this.quantidadeExperienciaPorAtaque = quantidadeExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioDTO getInventario() {
        return inventario;
    }

    public void setInventario(InventarioDTO inventario) {
        this.inventario = inventario;
    }

    public boolean isEscudoEquipado() {
        return escudoEquipado;
    }

    public void setEscudoEquipado(boolean escudoEquipado) {
        this.escudoEquipado = escudoEquipado;
    }
}
