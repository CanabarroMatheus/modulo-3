package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ItemEntity {

    @Id
    @SequenceGenerator(name = "ITEM_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_item")
    protected Integer id;

    @Column(nullable = false, unique = true)
    protected String descricao;

    @OneToMany(mappedBy = "item")
    private List<InventarioXItemEntity> inventarioItem;

    public ItemEntity() {
    }

    public ItemEntity(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<InventarioXItemEntity> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<InventarioXItemEntity> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
