package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioXItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemId;
import br.com.dbccompany.lotr.Service.InventarioXItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventarioXItem")
public class InventarioXItemController {

    @Autowired
    private InventarioXItemService service;

    @GetMapping
    @ResponseBody
    public List<InventarioXItemDTO> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/all-by-quantidade/{quantidade}")
    @ResponseBody
    public List<InventarioXItemDTO> findAllByQuantidade(@PathVariable Integer quantidade) {
        return this.service.findAllByQuantidade(quantidade);
    }

    @GetMapping("/all-by-inventario/{id}")
    @ResponseBody
    public List<InventarioXItemDTO> findAllByInventario(@PathVariable Integer id) {
        return this.service.findAllByInventario(id);
    }

    @GetMapping("/all-by-item/{id}")
    @ResponseBody
    public List<InventarioXItemDTO> findAllByItem(@PathVariable Integer id) {
        return this.service.findAllByItem(id);
    }

    @PostMapping("/by-id")
    @ResponseBody
    public InventarioXItemDTO findById(@RequestBody InventarioXItemId id) {
        return this.service.findById(id);
    }

    @GetMapping("/by-quantidade/{quantidade}")
    @ResponseBody
    public InventarioXItemDTO findByQuantidade(@PathVariable Integer quantidade) {
        return this.service.findByQuantidade(quantidade);
    }

    @GetMapping("/by-inventario/{id}")
    @ResponseBody
    public InventarioXItemDTO findByInventario(@PathVariable Integer id) {
        return this.service.findByInventario(id);
    }

    @GetMapping("/by-item/{id}")
    @ResponseBody
    public InventarioXItemDTO findByItem(@PathVariable Integer id) {
        return this.service.findByItem(id);
    }

    @PostMapping("/all-by-quantidades")
    @ResponseBody
    public List<InventarioXItemDTO> findAllByQuantidadeIn(@RequestBody List<Integer> quantidades) {
        return this.service.findAllByQuantidadeIn(quantidades);
    }

    @PostMapping("/all-by-inventarios")
    @ResponseBody
    public List<InventarioXItemDTO> findAllByInventarioIn(@RequestBody List<Integer> idsInventario) {
        return this.service.findAllByInventarioIn(idsInventario);
    }

    @PostMapping("/all-by-itens")
    @ResponseBody
    public List<InventarioXItemDTO> findAllByItemIn(@RequestBody List<Integer> idsItem) {
        return this.service.findAllByInventarioIn(idsItem);
    }

    @PostMapping
    @ResponseBody
    public InventarioXItemDTO save(@RequestBody InventarioXItemEntity inventarioXItemEntity) {
        return this.service.save(inventarioXItemEntity);
    }

//    @PutMapping("/{id}")
//    @ResponseBody
//    public InventarioXItemDTO update(@RequestBody InventarioXItemEntity inventarioXItemEntity, @PathVariable Integer id) {
//        return this.service.update(inventarioXItemEntity, id);
//    }
}
