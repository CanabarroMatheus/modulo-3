package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class InventarioEntity {

    @Id
    @SequenceGenerator(name = "INVENTARIO_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_inventario")
    protected Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_personagem")
    private PersonagemEntity personagem;

    @OneToMany(mappedBy = "inventario")
    private List<InventarioXItemEntity> inventarioItem;

    public InventarioEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonagemEntity getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemEntity personagem) {
        this.personagem = personagem;
    }

    public List<InventarioXItemEntity> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<InventarioXItemEntity> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }
}
