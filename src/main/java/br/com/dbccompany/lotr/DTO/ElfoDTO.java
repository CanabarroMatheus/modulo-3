package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;

public class ElfoDTO {
    private Integer id;
    private String nome;
    private Integer experiencia;
    private Double vida;
    private Double quantidadeDano;
    private Integer quantidadeExperienciaPorAtaque;
    private StatusEnum status;
    private InventarioDTO inventario;
    private Integer indiceFlecha;

    {
        this.vida = 100.0;
        this.inventario = new InventarioDTO();
        this.status = StatusEnum.RECEM_CRIADO;
        this.quantidadeExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.quantidadeDano = 0.0;
    }

    public ElfoDTO() {
    }

    public ElfoDTO(ElfoEntity elfo) {
        this.id = elfo.getId();
        this.nome = elfo.getNome();
        this.experiencia = elfo.getExperiencia();
        this.vida = elfo.getVida();
        this.quantidadeDano = elfo.getQuantidadeDano();
        this.quantidadeExperienciaPorAtaque = elfo.getQuantidadeExperienciaPorAtaque();
        this.status = elfo.getStatus();
        this.inventario = new InventarioDTO(elfo.getInventario());
        this.indiceFlecha = elfo.getIndiceFlecha();
    }

    public ElfoEntity converter() {
        ElfoEntity elfo = new ElfoEntity();
        elfo.setId(this.id);
        elfo.setNome(this.nome);
        elfo.setExperiencia(this.experiencia);
        elfo.setVida(this.vida);
        elfo.setQuantidadeDano(this.quantidadeDano);
        elfo.setQuantidadeExperienciaPorAtaque(this.quantidadeExperienciaPorAtaque);
        elfo.setStatus(this.status);
        if (this.inventario != null) {
            elfo.setInventario(this.inventario.converter());
        }
        elfo.setIndiceFlecha(this.indiceFlecha);
        return elfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQuantidadeDano() {
        return quantidadeDano;
    }

    public void setQuantidadeDano(Double quantidadeDano) {
        this.quantidadeDano = quantidadeDano;
    }

    public Integer getQuantidadeExperienciaPorAtaque() {
        return quantidadeExperienciaPorAtaque;
    }

    public void setQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque) {
        this.quantidadeExperienciaPorAtaque = quantidadeExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioDTO getInventario() {
        return inventario;
    }

    public void setInventario(InventarioDTO inventario) {
        this.inventario = inventario;
    }

    public Integer getIndiceFlecha() {
        return indiceFlecha;
    }

    public void setIndiceFlecha(Integer indiceFlecha) {
        this.indiceFlecha = indiceFlecha;
    }
}
