package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemId;

public class InventarioXItemDTO {
    private InventarioXItemId id;
    private Integer quantidade;
//    private InventarioDTO inventario;
    private ItemDTO item;

    public InventarioXItemDTO() {
    }

    public InventarioXItemDTO(InventarioXItemEntity inventarioXItemEntity) {
        this.id = inventarioXItemEntity.getId();
        this.quantidade = inventarioXItemEntity.getQuantidade();
        this.item = new ItemDTO(inventarioXItemEntity.getItem());
    }

    public InventarioXItemEntity converter() {
        InventarioXItemEntity inventarioXItemEntity = new InventarioXItemEntity();
        inventarioXItemEntity.setId(this.id);
        inventarioXItemEntity.setQuantidade(this.quantidade);
        inventarioXItemEntity.setItem(this.item.converter());
        return inventarioXItemEntity;
    }

    public InventarioXItemId getId() {
        return id;
    }

    public void setId(InventarioXItemId id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public ItemDTO getItem() {
        return item;
    }

    public void setItem(ItemDTO item) {
        this.item = item;
    }
}
