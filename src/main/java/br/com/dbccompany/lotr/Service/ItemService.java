package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    public List<ItemDTO> findAll() {
        List<ItemEntity> itens = (List<ItemEntity>) this.repository.findAll();
        return this.convertEntityListToDTOList(itens);
    }

    public List<ItemDTO> findAllByDescricao(String descricao) {
        List<ItemEntity> itens = this.repository.findAllByDescricao(descricao);
        return this.convertEntityListToDTOList(itens);
    }

    public List<ItemDTO> findAllByInventarioItem(InventarioXItemEntity inventarioXItemEntity) {
        List<ItemEntity> itens = this.repository.findAllByInventarioItem(inventarioXItemEntity);
        return this.convertEntityListToDTOList(itens);
    }

    public List<ItemDTO> findAllByDescricaoIn(List<String> descricoes) {
        List<ItemEntity> itens = this.repository.findAllByDescricaoIn(descricoes);
        return this.convertEntityListToDTOList(itens);
    }

    public List<ItemDTO> findAllByInventarioItemIn(List<InventarioXItemEntity> inventarioXItemEntities) {
        List<ItemEntity> itens = this.repository.findAllByInventarioItemIn(inventarioXItemEntities);
        return this.convertEntityListToDTOList(itens);
    }

    public ItemDTO findById(Integer id) {
        ItemEntity itemEntity = this.repository.findById(id).orElse(null);
        return new ItemDTO(itemEntity);
    }

    public ItemDTO findByDescricao(String descricao) {
        ItemEntity itemEntity = this.repository.findByDescricao(descricao).orElse(null);
        return new ItemDTO(itemEntity);
    }

    public ItemDTO findByInventarioItem(InventarioXItemEntity inventarioXItemEntity) {
        ItemEntity itemEntity = this.repository.findByInventarioItem(inventarioXItemEntity).orElse(null);
        return new ItemDTO(itemEntity);
    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO save(ItemEntity itemEntity) {
        ItemEntity itemNovo = this.saveAndUpdate(itemEntity);
        return new ItemDTO(itemNovo);
    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO update(ItemEntity itemEntity, int id) {
        itemEntity.setId(id);
        ItemEntity itemAtualizado = this.saveAndUpdate(itemEntity);
        return new ItemDTO(itemAtualizado);
    }

    private ItemEntity saveAndUpdate(ItemEntity itemEntity) {
        return this.repository.save(itemEntity);
    }

    public void delete(ItemEntity itemEntity) {
        this.repository.delete(itemEntity);
    }

    public void deleteById(Integer id) {
        this.repository.deleteById(id);
    }

    private List<ItemDTO> convertEntityListToDTOList(List<ItemEntity> itemEntities) {
        ArrayList<ItemDTO> itemDTOS = new ArrayList<>();

        for (ItemEntity itemEntity : itemEntities) {
            itemDTOS.add(new ItemDTO(itemEntity));
        }

        return itemDTOS;
    }

}
