package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Enum.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnaoService {

    @Autowired
    private AnaoRepository repository;

    public List<AnaoDTO> findAll() {
        List<AnaoEntity> anoes = (List<AnaoEntity>) this.repository.findAll();
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByNomeIn(List<String> nomes) {
        List<AnaoEntity> anoes = this.repository.findAllByNomeIn(nomes);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByExperienciaIn(List<Integer> experiencias) {
        List<AnaoEntity> anoes = this.repository.findAllByExperienciaIn(experiencias);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByVidaIn(List<Double> vida) {
        List<AnaoEntity> anoes = this.repository.findAllByVidaIn(vida);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByQuantidadeDanoIn(List<Double> quantidadesDano) {
        List<AnaoEntity> anoes = this.repository.findAllByQuantidadeDanoIn(quantidadesDano);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByQuantidadeExperienciaPorAtaqueIn(List<Integer> quantidadesExperienciaPorAtaque) {
        List<AnaoEntity> anoes = this.repository.findAllByQuantidadeExperienciaPorAtaqueIn(quantidadesExperienciaPorAtaque);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByStatusIn(List<StatusEnum> status) {
        List<AnaoEntity> anoes = this.repository.findAllByStatusIn(status);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByNome(String nome) {
        List<AnaoEntity> anoes = this.repository.findAllByNome(nome);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByExperiencia(Integer experiencia) {
        List<AnaoEntity> anoes = this.repository.findAllByExperiencia(experiencia);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByVida(Double vida) {
        List<AnaoEntity> anoes = this.repository.findAllByVida(vida);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByQuantidadeDano(Double quantidadeDano) {
        List<AnaoEntity> anoes = this.repository.findAllByQuantidadeDano(quantidadeDano);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque) {
        List<AnaoEntity> anoes = this.repository.findAllByQuantidadeExperienciaPorAtaque(quantidadeExperienciaPorAtaque);
        return this.convertEntityListToDTOList(anoes);
    }

    public List<AnaoDTO> findAllByStatus(StatusEnum status) {
        List<AnaoEntity> anoes = this.repository.findAllByStatus(status);
        return this.convertEntityListToDTOList(anoes);
    }

    public AnaoDTO findById(Integer id) {
        AnaoEntity anao = this.repository.findById(id).orElse(null);
        return new AnaoDTO(anao);
    }

    public AnaoDTO findByNome(String nome) {
        AnaoEntity anao = this.repository.findByNome(nome).orElse(null);
        return new AnaoDTO(anao);
    }

    public AnaoDTO findByExperiencia(Integer experiencia) {
        AnaoEntity anao = this.repository.findByExperiencia(experiencia).orElse(null);
        return new AnaoDTO(anao);
    }

    public AnaoDTO findByVida(Double vida) {
        AnaoEntity anao = this.repository.findByVida(vida).orElse(null);
        return new AnaoDTO(anao);
    }

    public AnaoDTO findByQuantidadeDano(Double quantidadeDano) {
        AnaoEntity anao = this.repository.findByQuantidadeDano(quantidadeDano).orElse(null);
        return new AnaoDTO(anao);
    }

    public AnaoDTO findByQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque) {
        AnaoEntity anao = this.repository.findByQuantidadeExperienciaPorAtaque(quantidadeExperienciaPorAtaque).orElse(null);
        return new AnaoDTO(anao);
    }

    public AnaoDTO findByStatus(StatusEnum status) {
        AnaoEntity anao = this.repository.findByStatus(status).orElse(null);
        return new AnaoDTO(anao);
    }

    @Transactional(rollbackFor = Exception.class)
    public AnaoDTO save(AnaoEntity anaoEntity) {
        AnaoEntity anao = this.saveAndUpdate(anaoEntity);
        return new AnaoDTO(anao);
    }

    @Transactional(rollbackFor = Exception.class)
    public AnaoDTO update(AnaoEntity anaoEntity, Integer id) {
        anaoEntity.setId(id);
        AnaoEntity anao = this.saveAndUpdate(anaoEntity);
        return new AnaoDTO(anao);
    }

    private AnaoEntity saveAndUpdate(AnaoEntity anaoEntity) {
        return this.repository.save(anaoEntity);
    }

    private List<AnaoDTO> convertEntityListToDTOList(List<AnaoEntity> anaoEntities) {
        ArrayList<AnaoDTO> anaoDTOS = new ArrayList<>();
        for (AnaoEntity anaoEntity : anaoEntities) {
            anaoDTOS.add(new AnaoDTO(anaoEntity));
        }
        return anaoDTOS;
    }
}
