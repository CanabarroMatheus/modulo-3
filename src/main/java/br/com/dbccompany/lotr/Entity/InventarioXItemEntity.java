package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
public class InventarioXItemEntity {
    @EmbeddedId
    private InventarioXItemId id;
    private Integer quantidade;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("id_inventario")
    private InventarioEntity inventario;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("id_item")
    private ItemEntity item;

    public InventarioXItemEntity() {
    }

    public InventarioXItemId getId() {
        return id;
    }

    public void setId(InventarioXItemId id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

    public ItemEntity getItem() {
        return item;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }
}
