package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
@DiscriminatorValue("Elfo")
public class ElfoEntity extends PersonagemEntity {
    private Integer indiceFlecha;

    {
        this.indiceFlecha = 1;
    }

    public ElfoEntity() {
        this.vida = 100.0;
    }

    public ElfoEntity(String nome) {
        super(nome);

        this.vida = 100.0;
    }

    public Integer getIndiceFlecha() {
        return indiceFlecha;
    }

    public void setIndiceFlecha(Integer indiceFlecha) {
        this.indiceFlecha = indiceFlecha;
    }
}
