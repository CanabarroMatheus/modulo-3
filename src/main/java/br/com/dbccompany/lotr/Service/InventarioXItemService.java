package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioXItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemId;
import br.com.dbccompany.lotr.Repository.InventarioXItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventarioXItemService {

    @Autowired
    private InventarioXItemRepository repository;

    public List<InventarioXItemDTO> findAll() {
        List<InventarioXItemEntity> inventarioXItemEntities = (List<InventarioXItemEntity>) this.repository.findAll();
        return this.convertEntityListToDTOList(inventarioXItemEntities);
    }

    public List<InventarioXItemDTO> findAllByQuantidade(Integer quantidade) {
        List<InventarioXItemEntity> inventarioXItemEntities = this.repository.findAllByQuantidade(quantidade);
        return this.convertEntityListToDTOList(inventarioXItemEntities);
    }

    public List<InventarioXItemDTO> findAllByInventario(Integer idInventario) {
        List<InventarioXItemEntity> inventarioXItemEntities = this.repository.findAllByInventario(idInventario);
        return this.convertEntityListToDTOList(inventarioXItemEntities);
    }

    public List<InventarioXItemDTO> findAllByItem(Integer idItem) {
        List<InventarioXItemEntity> inventarioXItemEntities = this.repository.findAllByItem(idItem);
        return this.convertEntityListToDTOList(inventarioXItemEntities);
    }

    public List<InventarioXItemDTO> findAllByQuantidadeIn(List<Integer> quantidades) {
        List<InventarioXItemEntity> inventarioXItemEntities = this.repository.findAllByQuantidadeIn(quantidades);
        return this.convertEntityListToDTOList(inventarioXItemEntities);
    }

    public List<InventarioXItemDTO> findAllByInventarioIn(List<Integer> idsInventario) {
        List<InventarioXItemEntity> inventarioXItemEntities = this.repository.findAllByInventarioIn(idsInventario);
        return this.convertEntityListToDTOList(inventarioXItemEntities);
    }

    public List<InventarioXItemDTO> findAllByItemIn(List<Integer> idsItem) {
        List<InventarioXItemEntity> inventarioXItemEntities = this.repository.findAllByItemIn(idsItem);
        return this.convertEntityListToDTOList(inventarioXItemEntities);
    }

    public InventarioXItemDTO findById(InventarioXItemId id) {
        InventarioXItemEntity inventarioXItemEntity = this.repository.findById(id).orElse(null);
        return new InventarioXItemDTO(inventarioXItemEntity);
    }

    public InventarioXItemDTO findByQuantidade(Integer quantidade) {
        InventarioXItemEntity inventarioXItemEntity = this.repository.findByQuantidade(quantidade).orElse(null);
        return new InventarioXItemDTO(inventarioXItemEntity);
    }

    public InventarioXItemDTO findByInventario(Integer idInventario) {
        InventarioXItemEntity inventarioXItemEntity = this.repository.findByInventario(idInventario).orElse(null);
        return new InventarioXItemDTO(inventarioXItemEntity);
    }

    public InventarioXItemDTO findByItem(Integer idItem) {
        InventarioXItemEntity inventarioXItemEntity = this.repository.findByItem(idItem).orElse(null);
        return new InventarioXItemDTO(inventarioXItemEntity);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioXItemDTO save(InventarioXItemEntity inventarioXItemEntity) {
        InventarioXItemEntity inventarioXItem = this.saveAndUpdate(inventarioXItemEntity);
        return new InventarioXItemDTO(inventarioXItem);
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioXItemDTO update(InventarioXItemEntity inventarioXItemEntity, InventarioXItemId id) {
        inventarioXItemEntity.setId(id);
        InventarioXItemEntity inventarioXItem = this.saveAndUpdate(inventarioXItemEntity);
        return new InventarioXItemDTO(inventarioXItem);
    }

    private InventarioXItemEntity saveAndUpdate(InventarioXItemEntity inventarioXItemEntity) {
        return this.repository.save(inventarioXItemEntity);
    }

    public void delete(InventarioXItemEntity inventarioXItemEntity) {
        this.repository.delete(inventarioXItemEntity);
    }

    public void deleteById(InventarioXItemId inventarioXItemId) {
        this.repository.deleteById(inventarioXItemId);
    }

    private List<InventarioXItemDTO> convertEntityListToDTOList(List<InventarioXItemEntity> inventarioXItemEntities) {
        ArrayList<InventarioXItemDTO> inventarioXItemDTOS = new ArrayList<>();

        for (InventarioXItemEntity inventarioXItemEntity: inventarioXItemEntities) {
            inventarioXItemDTOS.add(new InventarioXItemDTO(inventarioXItemEntity));
        }

        return inventarioXItemDTOS;
    }

}
