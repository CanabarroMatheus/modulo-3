package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.InventarioXItemEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventario")
public class InventarioController {

    @Autowired
    private InventarioService service;

    @GetMapping
    @ResponseBody
    public List<InventarioDTO> findAll() {
        return this.service.findAll();
    }

    @PostMapping("/all-by-personagem")
    @ResponseBody
    public List<InventarioDTO> findAllByPersonagem(@RequestBody PersonagemEntity personagem) {
        return this.service.findAllByPersonagem(personagem);
    }

    @PostMapping("/all-by-inventarioItem")
    @ResponseBody
    public List<InventarioDTO> findAllByInventarioItem(@RequestBody InventarioXItemEntity inventarioXItemEntity) {
        return this.service.findAllByInventarioItem(inventarioXItemEntity);
    }

    @PostMapping("/all-by-personagens")
    @ResponseBody
    public List<InventarioDTO> findAllByPersonagemIn(@RequestBody List<PersonagemEntity> personagens) {
        return this.service.findAllByPersonagemIn(personagens);
    }

    @PostMapping("/all-by-inventariosItem")
    @ResponseBody
    public List<InventarioDTO> findAllByInventarioItemIn(@RequestBody List<InventarioXItemEntity> inventariosItens) {
        return this.service.findAllByInventarioItemIn(inventariosItens);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public InventarioDTO findById(@PathVariable Integer id) {
        return this.service.findById(id);
    }

    @PostMapping("/by-personagem")
    @ResponseBody
    public InventarioDTO findByPersonagem(@RequestBody PersonagemEntity personagem) {
        return this.service.findByPersonagem(personagem);
    }

    @PostMapping("/by-inventarioItem")
    @ResponseBody
    public InventarioDTO findByInventarioItem(@RequestBody InventarioXItemEntity inventarioXItem) {
        return this.service.findByInventarioItem(inventarioXItem);
    }

    @PostMapping
    @ResponseBody
    public InventarioDTO save(@RequestBody InventarioDTO inventario) {
        return this.service.save(inventario.converter());
    }

    @PutMapping("/{id}")
    @ResponseBody
    public InventarioDTO update(@RequestBody InventarioEntity inventarioEntity, @PathVariable Integer id) {
        return this.update(inventarioEntity, id);
    }

}
