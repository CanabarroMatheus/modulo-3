package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnaoRepository extends PersonagemRepository<AnaoEntity> {
}
