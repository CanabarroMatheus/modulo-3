package br.com.dbccompany.lotr.Entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class InventarioXItemId implements Serializable {
    private Integer id_inventario;
    private Integer id_item;

    public InventarioXItemId() {
    }

    public InventarioXItemId(int id_inventario, int id_item) {
        this.id_inventario = id_inventario;
        this.id_item = id_item;
    }
}
