package br.com.dbccompany.lotr.Entity;

import br.com.dbccompany.lotr.Enum.StatusEnum;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo_personagem", discriminatorType = DiscriminatorType.STRING)
public abstract class PersonagemEntity {

    @Id
    @SequenceGenerator(name = "PERSONAGEM_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_personagem")
    private Integer id;

    @Column(nullable = false, unique = true)
    protected String nome;

    @Column(nullable = false)
    protected Integer experiencia;

    @Column(nullable = false, precision = 4, scale = 2)
    protected Double vida;

    @Column(nullable = false, precision = 4, scale = 2)
    protected Double quantidadeDano;

    @Column(nullable = false)
    protected Integer quantidadeExperienciaPorAtaque;

    @Enumerated(EnumType.STRING)
    protected StatusEnum status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_inventario", referencedColumnName = "id_inventario")
    protected InventarioEntity inventario;

    public PersonagemEntity() {
    }

    public PersonagemEntity(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getQuantidadeDano() {
        return quantidadeDano;
    }

    public void setQuantidadeDano(Double quantidadeDano) {
        this.quantidadeDano = quantidadeDano;
    }

    public Integer getQuantidadeExperienciaPorAtaque() {
        return quantidadeExperienciaPorAtaque;
    }

    public void setQuantidadeExperienciaPorAtaque(Integer quantidadeExperienciaPorAtaque) {
        this.quantidadeExperienciaPorAtaque = quantidadeExperienciaPorAtaque;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}
